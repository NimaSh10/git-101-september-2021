# README #

The files in this repository are being used for the September 2021 Git 101 workshop for Goanna Solutions.

Workshop participants may download the exercise or use the files to follow along during the workshop.

See `LICENSE.txt` for more information.

## Setup ##

Workshop attendees wishing to follow along with the presentation need to complete the OS-specific setup instructions in the `resources` folder.

These instructions will ensure that you have:

1. Created a free Bitbucket account via https://bitbucket.org/account/signup/
1. Installed prerequisite software, including Git, a text editor, and Git Bash
1. Setup SSH for Bitbucket
1. Confirmed your SSH setup is working

## Workshop activities ##

The `resources` folder contains a workshop activities handout. Download this handout prior to the workshop. Note that if you follow the setup instructions above (or sync a fork created during the workshop), the documents will be available in your fork.